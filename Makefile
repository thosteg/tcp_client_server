CFLAGS := -Wall -Werror -std=gnu11
#CFLAGS := -Wall -Werror 

SRCS := tcp_client.c tcp_server.c

OBJS := ${SRCS:c=o} 
PROGS := ${SRCS:.c=}

.PHONY: all
all: ${PROGS}

${PROGS} : % : %.o Makefile
	${CC} $< -o $@

clean:
	rm -f ${PROGS} ${OBJS}

%.o: %.c Makefile
	${CC} ${CFLAGS} -c $<
