# tcp_client_server

Goals: document as it is - even if ugly.

Source of this code was `man epoll`, fooling around, together with various searching on the internet in my free time. As learning tends to happen.

## Building

```
cc -Wall -o tcp_server tcp_server.c
cc -Wall -o tcp_client tcp_client.c
```

## Usage

```
./tcp_server
```

This will start a tcp-echo server on port `3333`.

Connecting to it:

```
./tcp_client
```

The client will write a message to the server in fixed intervals, until either is closed.
